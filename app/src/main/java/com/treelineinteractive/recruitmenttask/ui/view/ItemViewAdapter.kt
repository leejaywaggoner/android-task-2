package com.treelineinteractive.recruitmenttask.ui.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding

class ItemViewAdapter(private val list: List<ProductItem>) : RecyclerView.Adapter<ItemViewAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ViewProductItemBinding, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: ProductItem, position: Int) {
            binding.nameLabel.text = item.title
            binding.descriptionLabel.text = item.description
            binding.availableLabel.text =
                binding.availableLabel.context.getString(R.string.available_sold, (item.available - item.sold), item.sold)
            binding.sellButton.setOnClickListener {
                if (item.available != 0 && item.sold < item.available) {
                    item.sold++
                    binding.availableLabel.text =
                        binding.availableLabel.context.getString(R.string.available_sold, (item.available - item.sold), item.sold)
                    notifyItemChanged(position)
                }
            }
            binding.returnButton.setOnClickListener {
                if (item.sold > 0) {
                    item.sold--
                    binding.availableLabel.text =
                        binding.availableLabel.context.getString(R.string.available_sold, (item.available - item.sold), item.sold)
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewProductItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.bindView(item, position)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}