package com.treelineinteractive.recruitmenttask.ui

import android.content.Context
import android.content.Intent
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.repository.RepositoryRequestStatus
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainViewModel : BaseViewModel<MainViewModel.MainViewState, MainViewModel.MainViewAction>(MainViewState()) {

    var productItems: List<ProductItem>? = null

    data class MainViewState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val items: List<ProductItem> = listOf()
    ) : BaseViewState {
        val isSuccess: Boolean
            get() = !isLoading && error == null
    }

    sealed class MainViewAction : BaseAction {
        object LoadingProducts : MainViewAction()
        data class ProductsLoaded(val items: List<ProductItem>) : MainViewAction()
        data class ProductsLoadingError(val error: String) : MainViewAction()
    }

    private val shopRepository = ShopRepository()

    fun loadProducts() {
        GlobalScope.launch {
            shopRepository.getProducts()
                .collect { result ->
                    when (result.requestStatus) {
                        is RepositoryRequestStatus.FETCHING -> {
                            sendAction(MainViewAction.LoadingProducts)
                        }
                        is RepositoryRequestStatus.COMPLETE -> {
                            sendAction(MainViewAction.ProductsLoaded(result.data))
                        }
                        is RepositoryRequestStatus.Error -> {
                            sendAction(MainViewAction.ProductsLoadingError("Oops, something went wrong"))
                        }
                    }
                }
        }
    }

    override fun onReduceState(viewAction: MainViewAction): MainViewState = when (viewAction) {
        is MainViewAction.LoadingProducts -> state.copy(isLoading = true, error = null)
        is MainViewAction.ProductsLoaded -> state.copy(
            isLoading = false,
            error = null,
            items = viewAction.items
        )
        is MainViewAction.ProductsLoadingError -> state.copy(
            isLoading = false,
            error = viewAction.error
        )
    }

    fun sendEmail(context: Context) {
        productItems?.let { items ->
            var message = ""
            for (item in items) {
                message += context.getString(R.string.sales_line, item.id, item.title, item.sold)
            }
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(context.getString(R.string.boss_email)))
            intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.email_subject))
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.setType("message/rfc822")
            context.startActivity(Intent.createChooser(intent, "Choose an email client"))
        }
    }
}