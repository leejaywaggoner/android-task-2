package com.treelineinteractive.recruitmenttask.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.ui.view.ItemViewAdapter

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val mainViewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel.loadProducts()

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }

        mainViewModel.stateLiveData.observe { state ->
            binding.progressBar.isVisible = state.isLoading
            binding.errorLayout.isVisible = state.error != null
            binding.errorLabel.text = state.error

            binding.itemsRecyclerView.isVisible = state.isSuccess

            if (state.isSuccess) {
                mainViewModel.productItems = state.items
                binding.itemsRecyclerView.adapter = ItemViewAdapter(state.items)
            }
        }

        binding.emailBossman.setOnClickListener {
            mainViewModel.sendEmail(this@MainActivity)
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}